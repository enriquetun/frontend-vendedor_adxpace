import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/store/api'
import moment from 'moment'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    apiRoot: 'https://adxpace.test',
    authenticated: false,
    clientes: [],
    data_espacios: false,
    data_locales: false,
    data_institucionales: false,
    data_geo_localidad: false,
    geo_localidad_input: false,
    prueba_initmap: false,
    currentCliente: {},
    currentRelacion: {},
    cliente_relaciones: [],
    relacion_inventarios: false,
    data_giros: [],
    data_tipos: [],
    data_materiales: [],
    data_oootipo: [],
    data_url_espacio: '',
    data_url_local: '',
    data_url_institucional: ''
  },
  getters: {
    'inventarios': function (state) {
      var relacionInventarios = state.relacion_inventarios
      var inventarios = []
      for (var n = 0; n < relacionInventarios.length; n++) {
        var relacion = relacionInventarios[n]
        var inventario = {}
        if (relacion.relacion_tipo === 'E') {
          inventario.n = n
          inventario.pk_relacion = relacion.pk
          inventario.pk = relacion.espacio.pk
          inventario.latitud = relacion.espacio.geo_localidad.latitud
          inventario.longitud = relacion.espacio.geo_localidad.longitud
          inventario.foto = relacion.espacio.foto
          inventario.relacion_tipo = 'Espacio'
          inventario.name = relacion.espacio.nombre
          inventario.giro = relacion.espacio.giro.designacion
          inventario.tipo = relacion.espacio.tipo.designacion
          inventarios.push(inventario)
        } else if (relacion.relacion_tipo === 'L') {
          inventario.n = n
          inventario.pk_relacion = relacion.pk
          inventario.pk = relacion.local.pk
          inventario.latitud = relacion.local.geo_localidad.latitud
          inventario.longitud = relacion.local.geo_localidad.longitud
          inventario.foto = relacion.local.foto
          inventario.relacion_tipo = 'Local'
          inventario.name = relacion.local.nombre
          inventario.calle = relacion.local.direccion.calle
          inventario.colonia = relacion.local.direccion.colonia
          inventario.municipio = relacion.local.direccion.municipio
          inventarios.push(inventario)
        } else {
          inventario.n = n
          inventario.pk_relacion = relacion.pk
          inventario.pk = relacion.institucional.pk
          inventario.latitud = relacion.institucional.geo_localidad.latitud
          inventario.longitud = relacion.institucional.geo_localidad.longitud
          if (relacion.institucional.data.foto) {
            inventario.foto = relacion.institucional.data.foto[0]
          } else {
            inventario.foto = null
          }
          inventario.relacion_tipo = 'Institucional'
          inventario.name = relacion.institucional.data.empresa
          inventario.tipo = relacion.institucional.data.tipo
          inventario.calificacion = relacion.institucional.data.calificacion
          inventarios.push(inventario)
        }
      }
      return inventarios
    }
  },
  actions: {
    async comun (store) {
      await store.dispatch('giros')
      await store.dispatch('tipos')
      await store.dispatch('materiales')
      await store.dispatch('ooh_tipos')
    },
    async giros ({state, commit}) {
      var url = state.apiRoot + '/comun/giros/'
      return api.get(url)
        .then((response) => commit('GIROS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async tipos ({state, commit}) {
      var url = state.apiRoot + '/comun/tipos/'
      return api.get(url)
        .then((response) => commit('TIPOS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async materiales ({state, commit}) {
      var url = state.apiRoot + '/comun/materiales/'
      return api.get(url)
        .then((response) => commit('MATERIALES', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async ooh_tipos ({state, commit}) {
      var url = state.apiRoot + '/comun/ooh_tipos/'
      return api.get(url)
        .then((response) => commit('OOOTIPO', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async search_point ({state, commit}, direccion) {
      var url = state.apiRoot + '/comun/direccion/?direccion=' + direccion
      return api.get(url)
        .then((response) => commit('RESPONSE_POIN', response.body))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_cliente_relaciones ({state, commit}, pk) {
      var url = state.apiRoot + '/vendedor/clientes/' + pk
      return api.get(url)
        .then((response) => commit('GET_CLIENTE_RELACIONES', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_relacion_inventarios ({state, commit}, pk) {
      var url = state.apiRoot + '/vendedor/clientes/relaciones/' + pk
      return api.get(url)
        .then((response) => commit('GET_RELACIONES_INVENTARIOS', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_clientes ({state, commit}) {
      var url = state.apiRoot + '/vendedor/clientes/'
      return api.get(url)
        .then((response) => commit('GET_CLIENTES', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async get_cliente ({state, commit}, pk) {
      function getCliente (clientes) {
        return clientes.pk === pk
      }
      var currentCliente = state.clientes.find(getCliente)
      commit('SET_CURRENT_CLIENTE', currentCliente)
    },
    async get_relacion ({state, commit}, pk) {
      function getRelacion (clienteRelaciones) {
        return clienteRelaciones.pk === pk
      }
      var currentRelacion = state.cliente_relaciones.find(getRelacion)
      commit('SET_CURRENT_RELACION', currentRelacion)
    },
    // Posts
    async post_cliente ({state, commit}, postData) {
      var url = state.apiRoot + '/vendedor/clientes/'
      return api.post_json(url, postData)
        .then((response) => {
          commit('RESPONSE_POST_CLIENTE', response)
        })
        .catch((error) => commit('API_FAIL', error))
    },
    async post_inventario ({state, commit}, postInventario) {
      var url = state.apiRoot + '/vendedor/clientes/relaciones/' + state.currentRelacion.pk + '/'
      return api.post_json(url, postInventario)
    },
    async post_sucursal ({state, commit}, postData) {
      var url = state.apiRoot + '/vendedor/clientes/' + postData.cliente_json + '/'
      return api.post_json(url, postData)
        .then((response) => {
          commit('RESPONSE_ADD_SUCURSAL', response)
        })
        .catch((error) => commit('API_FAIL', error))
    },
    async post_usuario ({state, commit}, data) {
      var url = state.apiRoot + '/vendedor/user/'
      return api.post(url, data)
        .then((response) => commit('RESPONSE_USER', response))
        .catch((error) => commit('API_FAIL', error))
    },
    // Delete relacion inventarios
    async delete_relacion_inventario ({state, commit}, data) {
      var url = state.apiRoot + '/vendedor/clientes/relaciones/' + data.pk_relacion_cliente + '/?pk=' + data.pk_inventario
      return api.delete(url)
        .then((response) => commit('RESPONSE_DELETE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async delete_campana ({state, commit}, data) {
      var url = state.apiRoot + '/vendedor/clientes/' + data.relacion + '/?pk=' + data.valor_campana
      return api.delete(url)
        .then((response) => commit('RESPONSE_DELETE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async delete_cliente ({state, commit}, pkRelacion) {
      var url = state.apiRoot + '/vendedor/clientes/?pk=' + pkRelacion
      return api.delete(url)
        .then((response) => commit('RESPONSE_DELETE', response))
        .catch((error) => commit('API_FAIL', error))
    },
    // ------------- search Espacio ------------------------------
    async searchGeolocalidad ({state, commit}, search) {
      var url = state.apiRoot + '/comun/direccion/' + search
      return api.get(url)
        .then((response) => commit('RESPONSE_GEOLOCALIDAD_INPUT', response))
        .catch((error) => commit('API_FAIL', error))
    },
    async searchEspacios ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/vendedor/espacios/' + search
      }
      store.commit('URLDATA', url)
      return api.get(url)
        .then((response) => {
          var dataEspacios = response.body
          for (var n = 0; n < dataEspacios.results.length; n++) {
            dataEspacios.results[n].fecha_de_registro = moment(dataEspacios.results[n].fecha_de_registro)
          }
          store.commit('ESPACIOS_SEARCH', dataEspacios)
        }).catch((error) => commit('API_FAIL', error))
    },
    async searchLocales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/vendedor/locales/' + search
      }
      store.commit('URLDATALOCAL', url)
      return api.get(url)
        .then((response) => {
          var dataLocales = response.body
          for (var n = 0; n < dataLocales.results.length; n++) {
            dataLocales.results[n].fecha_de_registro = moment(dataLocales.results[n].fecha_de_registro)
          }
          store.commit('LOCALES_SEARCH', dataLocales)
        }).catch((error) => commit('API_FAIL', error))
    },
    async searchInstitucionales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/vendedor/espacios_institucionales/' + search
      }
      store.commit('URLDATAINSTITUCIONAL', url)
      return api.get(url)
        .then((response) => {
          store.commit('INSTITUCIONALES_SEARCH', response)
        }).catch((error) => commit('API_FAIL', error))
    }
  },
  mutations: {
    RESPONSE_POST_CLIENTE: function (state, bodyCliente) {
      state.data_cliente_pk = bodyCliente.body.pk
    },
    URLDATA: function (state, url) {
      state.data_url_espacio = url
    },
    URLDATALOCAL: function (state, url) {
      state.data_url_local = url
    },
    URLDATAINSTITUCIONAL: function (state, url) {
      state.data_url_institucional = url
    },
    GIROS: function (state, giros) {
      state.data_giros = giros.body
    },
    TIPOS: function (state, tipos) {
      state.data_tipos = tipos.body
    },
    MATERIALES: function (state, materiales) {
      state.data_materiales = materiales
    },
    OOOTIPO: function (state, tipo) {
      state.data_oootipo = tipo.body
    },
    SET_CURRENT_CLIENTE: function (state, currentCliente) {
      state.currentCliente = currentCliente
    },
    SET_CURRENT_RELACION: function (state, currentRelacion) {
      state.currentRelacion = currentRelacion
    },
    GET_CLIENTES: function (state, response) {
      state.clientes = response.body
    },
    GET_CLIENTE_RELACIONES: function (state, response) {
      // console.log(response.body)
      state.cliente_relaciones = response.body
    },
    GET_RELACIONES_INVENTARIOS: function (state, response) {
      state.relacion_inventarios = response.body
    },
    RESPONSE_ADD_SUCURSAL: function (state, response) {
      state.response_sucursal = response.body.pk
    },
    ESPACIOS_SEARCH: function (state, dataEspacios) {
      // console.log(dataEspacios)
      state.data_espacios = dataEspacios
    },
    LOCALES_SEARCH: function (state, dataLocales) {
      console.log(dataLocales)
      state.data_locales = dataLocales
    },
    INSTITUCIONALES_SEARCH: function (state, response) {
      state.data_institucionales = response.body
      console.log(response.body)
    },
    GEO_LOCALIDAD: function (state, response) {
      state.data_geo_localidad = response
    },
    GEO_LOCALIDAD_RESET: function (state) {
      state.data_geo_localidad = []
    },
    RESET_TABLE: function (state, response) {
      // state.data_espacios = []
      state.data_locales = []
      state.data_institucionales = []
    },
    RESET_TABLE_ESPACIOS: function (state) {
      state.data_espacios = []
    },
    RESET_POIN: function (state) {
      state.prueba_initmap = []
    },
    RESPONSE_POIN: function (state, response) {
      state.prueba_initmap = response
    },
    RESPONSE_GEOLOCALIDAD_INPUT: function (state, response) {
      state.geo_localidad_input = response.body
    },
    RESPONSE_USER: function (state, response) {
      // console.log(response.body)
      state.response_user = response.body
    },
    RESPONSE_DELETE: function (state, response) {
      console.log(response)
    },
    API_FAIL (state, error) {
      console.error(error)
    }
  }
})

export default store
