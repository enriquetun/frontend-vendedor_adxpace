import Vue from 'vue'
import App from './App'
import router from './router/index'
import store from './store/store'
import ClientTable from 'vue-tables-2'
import moment from 'moment'

Vue.config.productionTip = false

Vue.use(ClientTable)

Vue.prototype.moment = moment

require('moment/moment.js')
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})

require('@/assets/scss/app.scss')

try {
  window.$ = window.jQuery = require('jquery')
  require('bootstrap')
  require('exif')
} catch (e) {

}
