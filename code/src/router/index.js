import Vue from 'vue'
import Router from 'vue-router'

import Clientes from '@/components/Clientes'
import TablaSucursales from '@/components/TablaSucursales'
import TablaSucursalEspacios from '@/components/TablaSucursalEspacios'
import RegistroCliente from '@/components/RegistroCliente'
import ClienteAddSucursal from '@/components/ClienteAddSucursal'
import Incidencias from '@/components/Incidencias'
import VendedorEspacio from '@/components/VendedorEspacio'
import AgregarUsuario from '@/components/AgregarUsuario'
import tablaLocales from '@/components/tablaLocales'
import tablaEspacios from '@/components/tablaEspacios'
import tablaEspaciosInstitucionales from '@/components/tablaEspaciosInstitucionales'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Clientes',
      component: Clientes
    },
    {
      path: '/cliente',
      name: 'TablaSucursales',
      component: TablaSucursales
    },
    {
      path: '/cliente/sucursal/espacios',
      name: 'SucursalEspacios',
      component: TablaSucursalEspacios
    },
    {
      path: '/cliente/registro',
      name: 'ClienteRegistro',
      component: RegistroCliente
    },
    {
      path: '/cliente/addsucursal',
      name: 'ClienteAddSucursal',
      component: ClienteAddSucursal
    },
    {
      path: '/incidencias',
      name: 'Incidencias',
      component: Incidencias
    },
    {
      path: '/cliente/sucursal/espacios/agregar',
      name: 'VendedorEspacio',
      component: VendedorEspacio
    },
    {
      path: '/cliente/addsucursal/usuario',
      name: 'AgregarUsuario',
      component: AgregarUsuario
    },
    // rutas de las tablas
    {
      path: '/cliente/sucursal/espacios/locales',
      name: 'tablaLocales',
      component: tablaLocales
    },
    {
      path: '/cliente/sucursal/espacios/espacios',
      name: 'TablaEspacios',
      component: tablaEspacios
    },
    {
      path: '/cliente/sucursal/espacios/institucionales',
      name: 'tablaEspaciosInstitucionale',
      component: tablaEspaciosInstitucionales
    }
  ]
})
